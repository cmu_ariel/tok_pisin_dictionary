# -*- coding: utf-8 -*-
"""
06 Jun 2019
Convert .db files into TSV
"""

# Import statements
from __future__ import print_function, division
import sys
sys.path.append('/usr2/home/amuis/codebase/sf/utils')
from argparse import ArgumentParser
import re
import codecs

def main(args=None):
    parser = ArgumentParser(description='Convert .db files into .tsv')
    parser.add_argument('--inpath', default='toolbox/Mendelexicon.db')
    parser.add_argument('--outpath', default='lexicon_tpi_mendelexicon.tsv')
    args = parser.parse_args(args)

    def clean(word):
        word = word.replace('_', ' ')
        word = word.strip('\'"')
        word = re.sub(r'\([^)]+(\))?', '', word)
        word = word.strip()
        words = word.split(';')
        words = [word.strip() for word in words]
        words = [word for word in words if word]
        return words

    entries = {}
    with codecs.open(args.inpath, 'r', encoding='latin') as infile:
        entry_proto = {}
        for line in infile.readlines():
            if line.strip() == '':
                if 'ge' in entry_proto and 'gn' in entry_proto:
                    en_word = entry_proto['ge']
                    il_word = entry_proto['gn']
                elif 'ce' in entry_proto and 'cn' in entry_proto:
                    en_word = entry_proto['ce']
                    il_word = entry_proto['cn']
                else:
                    en_word = il_word = None
                entry_proto = {}
                if not en_word:
                    continue
                en_words = clean(en_word)
                il_words = clean(il_word)
                for en_word in en_words:
                    if en_word not in entries:
                        entries[en_word] = set()
                    for il_word in il_words:
                        entries[en_word].add(il_word)
            else:
                tokens = line.strip().split(' ', 1)
                if len(tokens) == 1:
                    # Not an entry line
                    continue
                key = tokens[0][1:]
                value = tokens[1]
                entry_proto[key] = value
    with open(args.outpath, 'w') as outfile:
        for en_word, il_words in sorted(entries.items()):
            for il_word in sorted(il_words):
                outfile.write('{}\t{}\n'.format(en_word, il_word))

if __name__ == '__main__':
    main()

