# -*- coding: utf-8 -*-
"""
24 May 2019
To fetch dictionary from tok-pisin.com
"""

# Import statements
from __future__ import print_function, division
import sys
sys.path.append('/usr2/home/amuis/codebase/sf/utils')
from argparse import ArgumentParser
from pathlib import Path
import requests
from bs4 import BeautifulSoup as BS
import re

def main(args=None):
    parser = ArgumentParser(description='To fetch dictionary from tok-pisin.com')
    parser.add_argument('--outfile',
                        help='The output file')
    args = parser.parse_args(args)
    files_dir = '__tokpisin_files'
    p = Path(files_dir)
    try:
        p.mkdir()
    except:
        pass

    baseurl = 'https://www.tok-pisin.com/search-results.php?q={}&select=english&Submit=go'
    vocab = {}
    for letter in 'abcdefghijklmnopqrstuvwxyz':
        url = baseurl.format(letter)
        filename = '{}/{}.html'.format(files_dir, letter)
        if not Path(filename).exists():
            text = requests.get(url).text
            with open(filename, 'w') as outfile:
                outfile.write(text)
        else:
            with open(filename, 'r' ) as infile:
                text = infile.read()
        soup = BS(text, 'lxml')
        for tr in soup.find_all('tr'):
            try:
                en_word, il_word = tr.find_all('td')
            except:
                continue
            vocab[en_word.text] = il_word.text

    def clean(word):
        word = re.sub(r'\([^)]+(\))?', '', word)
        word = word.strip()
        words = word.split(',')
        words = [word.strip() for word in words]
        words = [word for word in words if word]
        return words

    word_list = []
    for en_word, il_word in vocab.items():
        clean_en_words = clean(en_word)
        clean_il_words = clean(il_word)
        for clean_en_word in clean_en_words:
            for clean_il_word in clean_il_words:
                word_list.append((clean_en_word, clean_il_word))
    word_list = sorted(word_list)
    with open(args.outfile, 'w') as outfile:
        for en_word, il_word in word_list:
            outfile.write('{}\t{}\n'.format(en_word, il_word))

if __name__ == '__main__':
    main()

