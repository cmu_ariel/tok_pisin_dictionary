# -*- coding: utf-8 -*-
"""
24 May 2019
To fetch dictionary from wiktionary.org
"""

# Import statements
from __future__ import print_function, division
import sys
sys.path.append('/usr2/home/amuis/codebase/sf/utils')
from argparse import ArgumentParser
from pathlib import Path
import requests
from bs4 import BeautifulSoup as BS
import re
from tqdm import tqdm

def main(args=None):
    parser = ArgumentParser(description='To fetch dictionary from wiktionary.org')
    parser.add_argument('--outfile', default='lexicon_tpi_wiktionary.tsv',
                        help='The output file')
    parser.add_argument('--raw', action='store_true',
                        help='Whether to not attempt to clean the translation')
    args = parser.parse_args(args)
    files_dir = 'wiktionary_files'
    p = Path(files_dir)
    try:
        p.mkdir()
    except:
        pass

    # Get the list of words
    start_word = 'abababa'
    start_url = 'https://en.wiktionary.org/w/index.php?title=Category:Tok_Pisin_lemmas&pagefrom={}'
    il_words = []
    while start_word:
        url = start_url.format(start_word)
        filename = '{}/__page_{}.html'.format(files_dir, start_word)
        if not Path(filename).exists():
            text = requests.get(url).text
            with open(filename, 'w') as outfile:
                outfile.write(text)
        else:
            with open(filename, 'r' ) as infile:
                text = infile.read()
        soup = BS(text, 'lxml')
        a_objs = soup.find_all('a', attrs={'title':'Category:Tok Pisin lemmas'})
        a_obj = None
        for _a_obj in a_objs:
            if 'pagefrom' in _a_obj['href']:
                a_obj = _a_obj
                break
        if a_obj:
            url = a_obj['href']
            start_word = url[url.find('%0A')+3:url.rfind('#')]
        else:
            start_word = None
        for li in a_objs[0].parent.find_all('li'):
            il_words.append(li.text)

    baseurl = 'https://en.wiktionary.org/wiki/{}'
    vocab = {}
    for il_word in tqdm(il_words, smoothing=0.1):
        url = baseurl.format(il_word)
        filename = '{}/{}.html'.format(files_dir, il_word)
        if not Path(filename).exists():
            text = requests.get(url).text
            with open(filename, 'w') as outfile:
                outfile.write(text)
        else:
            with open(filename, 'r' ) as infile:
                text = infile.read()
        soup = BS(text, 'lxml')
        # Find the Tok Pisin section
        tpi_span = soup.find('span', attrs={'id':'Tok_Pisin'})
        dom = tpi_span.parent
        while dom.name != 'hr':
            dom = dom.next_sibling
            if not dom:
                break
            if dom.name == 'ol':
                for li in dom.find_all('li'):
                    for child in li.children:
                        if child.name in ['ul']:
                            child.decompose()
                    en_word = li.text
                    vocab[en_word] = il_word

    if args.raw:
        def clean(word):
            return [word.replace('\n', ' ').strip()]
    else:
        def clean(word):
            word = word.replace('\n', ' ')
            word = re.sub(r'\([^)]+(\))?', '', word)
            word = word.strip()
            word = word.strip('.')
            comma_words = word.split(',')
            if any(len(comma_word.split()) > 4 for comma_word in comma_words):
                words = word.split(';')
            else:
                words = re.split('[,;]', word)
            words = [word.strip() for word in words]
            words = [word for word in words if word]
            return words

    word_list = []
    for en_word, il_word in vocab.items():
        clean_en_words = clean(en_word)
        clean_il_words = clean(il_word)
        for clean_en_word in clean_en_words:
            if not args.raw and len(clean_en_word.split()) > 4:
                continue
            for clean_il_word in clean_il_words:
                if not args.raw and len(clean_il_word.split()) > 4:
                    continue
                word_list.append((clean_en_word, clean_il_word))
    word_list = sorted(word_list)
    with open(args.outfile, 'w') as outfile:
        for en_word, il_word in word_list:
            outfile.write('{}\t{}\n'.format(en_word, il_word))

if __name__ == '__main__':
    main()

