# -*- coding: utf-8 -*-
"""
06 Jun 2019
To merge various dictionaries
"""

# Import statements
from __future__ import print_function, division
import sys
sys.path.append('/usr2/home/amuis/codebase/sf/utils')
from argparse import ArgumentParser

def main(args=None):
    parser = ArgumentParser(description='To merge various dictionaries')
    parser.add_argument('--inpaths', nargs='+', default=[],
                        help='The list of TSV')
    parser.add_argument('--outpath', default='lexicon_tpi_all.tsv')
    args = parser.parse_args(args)

    entries = {}
    for inpath in args.inpaths:
        with open(inpath, 'r') as infile:
            for line in infile:
                en_word, il_word = line.strip().split('\t')[:2]
                if en_word not in entries:
                    entries[en_word] = set()
                entries[en_word].add(il_word)
    with open(args.outpath, 'w') as outfile:
        for en_word, il_words in sorted(entries.items()):
            for il_word in sorted(il_words):
                outfile.write('{}\t{}\n'.format(en_word, il_word))

if __name__ == '__main__':
    main()

