# 06 Jun 2019
# By: Aldrian Obaja Muis
# Run db_to_tsv.py

python3 db_to_tsv.py \
    --inpath "toolbox/Mendelexicon.db" \
    --outpath "lexicon_tpi_mendelexicon.tsv"

python3 db_to_tsv.py \
    --inpath "toolbox/Oksapmin.db" \
    --outpath "lexicon_tpi_oksapmin.tsv"
