# -*- coding: utf-8 -*-
"""
11 Jun 2019
Here goes the program description
"""

# Import statements
from __future__ import print_function, division
import sys
sys.path.append('/usr2/home/amuis/codebase/sf/utils')
from argparse import ArgumentParser
import pickle
from pprint import pprint
import re

def main(args=None):
    parser = ArgumentParser(description='')
    parser.add_argument('--dict_inpath', default='dictionary_entries.pkl',
                        help='The input dictionary entries')
    parser.add_argument('--header_inpath', default='header2langid.pkl',
                        help='The header, labeling each word as English or Tok Pisin')
    parser.add_argument('--sent_inpath', default='example_sentences.pkl',
                        help='The pickle file containing parallel sentences')
    parser.add_argument('--lexicon_outpath', default='lexicon_tpi_tokpisin_info.tsv',
                        help='The output dictionary file')
    parser.add_argument('--parallel_outpath', default='parallel_tpi_tokpisin_info.cdec',
                        help='The output parallel sentences')
    args = parser.parse_args(args)
    with open(args.dict_inpath, 'rb') as infile:
        word_to_translation = pickle.load(infile)
    with open(args.header_inpath, 'rb') as infile:
        word_to_lang = pickle.load(infile)
    eng_wordset = set()
    tpi_wordset = set()
    for word, lang in word_to_lang.items():
        if 'English' in lang:
            eng_wordset.add(word)
        else:
            tpi_wordset.add(word)
    pprint(sorted(eng_wordset))
    pprint(sorted(tpi_wordset))
    pprint(word_to_translation)
    pprint(word_to_lang)
    def clean_eng(word):
        if word.startswith('See also:') or word[-1] in '.:?':
            return []
        word = word.replace('\n', ' ')
        word = re.sub(r'(\ufeff|\x0a)', '', word) # Remove weird characters
        word = re.sub(r'\([^)]+(\))?', '', word) # Remove parentheses
        word = re.sub('(noun|pronoun|verb|adjective|adverb|preposition)', '', word)
        word = re.sub(r'\d+\.', '', word) # Remove numbering
        word = word.strip('\t .')
        comma_words = word.split(',')
        if any(len(comma_word.split()) > 4 for comma_word in comma_words):
            words = word.split(';')
        else:
            words = re.split('[,;]', word)
        words = [re.sub('^(to( be)?|a|be) ', '', word.strip('\t .')).strip() for word in words]
        words = [re.sub(' +', ' ', word) for word in words if word and 'FYI' not in word]
        return words

    def clean_tpi(word):
        words = clean_eng(word)
        return words

    eng_to_tpi = {}
    for word, translations in word_to_translation.items():
        if word in eng_wordset:
            eng_words = clean_eng(word)
            tpi_words = [w for trans in translations for w in clean_tpi(trans)]
        else:
            tpi_words = clean_tpi(word)
            eng_words = [w for trans in translations for w in clean_eng(trans)]
        for eng_word in eng_words:
            if eng_word not in eng_to_tpi:
                eng_to_tpi[eng_word] = set()
            for tpi_word in tpi_words:
                eng_to_tpi[eng_word].add(tpi_word)

    with open(args.lexicon_outpath, 'w') as outfile:
        for eng_word, tpi_words in sorted(eng_to_tpi.items()):
            for tpi_word in sorted(tpi_words):
                outfile.write('{}\t{}\n'.format(eng_word, tpi_word))

    with open(args.sent_inpath, 'rb') as infile:
        word_to_sentences = pickle.load(infile)
    pprint(word_to_sentences)
    
    def fix_pair(sentence1, sentence2):
        """Some sentences have portion of the other language coming
        before newline. Extract them and put at the end of the other language."""
        rightmost_newline = sentence1.rfind('\n')
        if rightmost_newline >= 0:
            other_language = sentence1[:rightmost_newline+1].strip()
            sentence2 = (sentence2 + ' ' + other_language).replace('  ', ' ')
            sentence1 = sentence1[rightmost_newline+1:].strip()
        return sentence1, sentence2

    eng_tpi_parallel = []
    for word, sentence_pairs in sorted(word_to_sentences.items()):
        if word in eng_wordset:
            eng_idx, tpi_idx = 0, 1
        else:
            eng_idx, tpi_idx = 1, 0
        for sentence_pair in sorted(sentence_pairs):
            eng_sentence = sentence_pair[eng_idx]
            tpi_sentence = sentence_pair[tpi_idx]
            eng_sentence, tpi_sentence = fix_pair(eng_sentence, tpi_sentence)
            tpi_sentence, eng_sentence = fix_pair(tpi_sentence, eng_sentence)
            eng_tpi_parallel.append((eng_sentence, tpi_sentence))

    with open(args.parallel_outpath, 'w') as outfile:
        for eng_sentence, tpi_sentence in eng_tpi_parallel:
            outfile.write('{} ||| {}\n'.format(tpi_sentence, eng_sentence))

if __name__ == '__main__':
    main()

